/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.eva02.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import root.eva02.DAO.ArticuloJpaController;
import root.eva02.DAO.exceptions.NonexistentEntityException;
import root.eva02.entity.Articulo;

/**
 *
 * @author surro
 */
@WebServlet(name = "ArticuloController", urlPatterns = {"/ArticuloController"})
public class ArticuloController extends HttpServlet {

  /**
   * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
   * methods.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  protected void processRequest(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    response.setContentType("text/html;charset=UTF-8");
    try ( PrintWriter out = response.getWriter()) {
      /* TODO output your page here. You may use following sample code. */
      out.println("<!DOCTYPE html>");
      out.println("<html>");
      out.println("<head>");
      out.println("<title>Servlet ArticuloController</title>");
      out.println("</head>");
      out.println("<body>");
      out.println("<h1>Servlet ArticuloController at " + request.getContextPath() + "</h1>");
      out.println("</body>");
      out.println("</html>");
    }
  }

  // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
  /**
   * Handles the HTTP <code>GET</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    try {
      ArticuloJpaController dao = new ArticuloJpaController();
      String seleccion = request.getParameter("btnseleccion");
      String radio = request.getParameter("seleccionid");
      switch (seleccion) {
        case "Agregar":
          request.getRequestDispatcher("index.jsp").forward(request, response);
          
        case "Volver":
          request.getRequestDispatcher("index.jsp").forward(request, response);
          
        case "Editar":
          Articulo ar = dao.findArticulo(radio);
          request.setAttribute("articulo", ar);
          request.getRequestDispatcher("modificar.jsp").forward(request, response);
          break;
          
        case "Eliminar":
          dao.destroy(radio);
          request.getRequestDispatcher("listar.jsp").forward(request, response);
      }
    } catch (NonexistentEntityException ex) {
      Logger.getLogger(ArticuloController.class.getName()).log(Level.SEVERE, null, ex);
    }

  }

  /**
   * Handles the HTTP <code>POST</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {

    ArticuloJpaController dao = new ArticuloJpaController();
    try {
      String id = request.getParameter("id");
      String nombre = request.getParameter("nombre");
      String tipo = request.getParameter("tipo");
      String descripcion = request.getParameter("descripcion");
      String precio = request.getParameter("precio");

      Articulo art = new Articulo();
      art.setId(id);
      art.setNombre(nombre);
      art.setTipo(tipo);
      art.setDescripcion(descripcion);
      art.setPrecio(precio);

      String btn = request.getParameter("btn");
      
      switch(btn){
        case "registrar":
          dao.create(art);
          break;
        case "modificar":
          dao.edit(art);
          break;
      }

    } catch (Exception ex) {
      Logger.getLogger(ArticuloController.class.getName()).log(Level.SEVERE, null, ex);
    }

    List<Articulo> listart = dao.findArticuloEntities();
    request.setAttribute("lista", listart);
    request.getRequestDispatcher("listar.jsp").forward(request, response);
  }

  /**
   * Returns a short description of the servlet.
   *
   * @return a String containing servlet description
   */
  @Override
  public String getServletInfo() {
    return "Short description";
  }// </editor-fold>

}
