<%-- 
    Document   : modificar
    Created on : 10-10-2021, 20:54:35
    Author     : surro
--%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="root.eva02.entity.Articulo"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
  Articulo ar = (Articulo) request.getAttribute("articulo");
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Modificar articulo</h1>
        <form name="form" action="ArticuloController" method="POST">
            <label>Ingrese id: <input type="number" id="id" name="id" value="<%= ar.getId() %>" autofocus required></label>
            <label>Ingrese nombre:  <input type="text" id="nombre" name="nombre" value="<%= ar.getNombre() %>"  required></label>
            <label>Ingrese tipo: <input type="text" id="tipo" name="tipo" value="<%= ar.getTipo() %>" required></label>
            <label>Ingrese descripcion: <input type="text" id="descripcion" name="descripcion" value="<%= ar.getDescripcion() %>" required></label>
            <label>Ingrese precio: <input type="number" id="precio" name="precio" value="<%= ar.getPrecio() %>" required></label>
            <button type="submit" name="btn" value="modificar">Modificar</button>
            
        </form>
    </body>
</html>
