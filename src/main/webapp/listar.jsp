<%-- 
    Document   : listar
    Created on : 10-10-2021, 13:34:39
    Author     : surro
--%>

<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="root.eva02.entity.Articulo"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
  List<Articulo> list = (List<Articulo>) request.getAttribute("lista");
  Iterator<Articulo> itlist = list.iterator();
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
            <form action="ArticuloController" method="GET">
            <table style=" border: 1px; text-align: center">
                <tr>
                    <th></th>
                    <th>ID</th>
                    <th>Nombre</th>
                    <th>Tipo</th>
                    <th>Descripcion</th>
                    <th>Precio</th>
                </tr>
                <% while (itlist.hasNext()) {
                Articulo next = itlist.next();%>
                <tr>
                    <td><input type="radio" name="seleccionid" value="<%= next.getId() %>"></td>
                    <td><%= next.getId()%></td>
                    <td><%= next.getNombre()%></td>
                    <td><%= next.getTipo()%></td>
                    <td><%= next.getDescripcion()%></td>
                    <td><%= next.getPrecio()%></td>
                </tr>
                <% }%>
            </table>
            <input type="submit" name="btnseleccion" value="Agregar">
            <input type="submit" name="btnseleccion" value="Editar">
            <input type="submit" name="btnseleccion" value="Eliminar">
            <input type="submit" name="btnseleccion" value="Volver">
        </form>
    </body>
</html>
