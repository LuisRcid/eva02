<%-- 
    Document   : index
    Created on : 10-10-2021, 13:08:05
    Author     : surro
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Agregar articulo</h1>
        <form name="form" action="ArticuloController" method="POST">
            <label>Ingrese id: <input type="number" id="id" name="id" autofocus required></label>
            <label>Ingrese nombre:  <input type="text" id="nombre" name="nombre"  required></label>
            <label>Ingrese tipo: <input type="text" id="tipo" name="tipo"  required></label>
            <label>Ingrese descripcion: <input type="text" id="descripcion" name="descripcion"  required></label>
            <label>Ingrese precio: <input type="number" id="precio" name="precio"  required></label>
            <button type="submit" name="btn" value="registrar">Registrar</button>
            
        </form>
    </body>
</html>
